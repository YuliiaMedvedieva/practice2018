#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <string.h>

void replace(char *line); //������� ������ �� �������� ";", ��������� ������ � �������� ���������
void filesearch(FILE *dbase, char *srch); //����� ������ � �����, ��������� ��������� �� ���� ���� ������ � ������ ������
void filesave(FILE *dbase, char *srch); //����� ������ � ����� � ����������� � ���������������� ����. ��������� ��������� �� ���� ���� ������ � ������ ������

int main(void)
{
	char srch[100];
	int choice;
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	FILE *dbase = fopen("perelik_vnz_data.csv", "r");
	printf("������� ������ ��� ������: ");
	scanf("%s", srch);
	printf("������ �� �� ��������� ��������� ������ � ����?\n1 - ��\n2 - ���\n");
	scanf("%i", &choice);
	if ((choice != 1) && (choice != 2))
	{
		printf("������. ������������ ����.\n");
		system("pause");
		return 1;
	}
	else if (choice == 1) filesave (dbase, srch);
	if (choice == 2) filesearch(dbase, srch);
	system("pause");
	return 0;
}

void replace (char *line)
{
	char flt = ';', plsh = ' ';
	int i = 0;
	while (line[i] != '\0')
	{
		if (line[i] == flt)
			line[i] = plsh;
		i++;
	}
}

void filesearch(FILE *dbase, char *srch)
{
	char line[500];
	while (!(feof(dbase)))
	{
		fgets(line, sizeof(line), dbase);
		if ((strstr(line, srch)) != NULL)
		{
			replace(line);
			puts(line);
		}
	}
}

void filesave(FILE *dbase, char *srch)
{
	char fname[30];
	char line[500];
	printf("������� �������� ����� � ����������� .csv: ");
	scanf("%s", fname);
	FILE *write = fopen(fname, "w");
	while (!(feof(dbase)))
	{
		fgets(line, sizeof(line), dbase);
		if ((strstr(line, srch)) != NULL)
		{
			fputs(line, write);
			puts(line);
		}
	}
}